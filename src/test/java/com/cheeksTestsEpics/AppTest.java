package com.cheeksTestsEpics;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.Arrays;

/**
 * Unit test for simple App.
 */
public class AppTest
    extends TestCase
{
    /**
     * Rigourous Test :-)
     */
    public void testBubbleSort()
    {
        int inputData[] = {5,4,3,2,1};
        int sortOutputData[] = {1,2,3,4,5};
        Sorter sorter = new Sorter();
        assertTrue(Arrays.equals(sorter.bubbleSort(inputData), sortOutputData));
    }
    public void testUnique () {
        int inputData[] = {1,1,2,3,4,5,2,3,4,5};
        int uniqueOutputData[] = {1,2,3,4,5};
        Unique unique = new Unique();
        assertTrue(Arrays.equals(unique.intUniqueList(inputData), uniqueOutputData));
    }
}
