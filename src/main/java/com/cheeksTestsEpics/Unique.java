package com.cheeksTestsEpics;

import java.util.ArrayList;

/**
 * Created by EnriqueLC on 02/10/2016.
 */
public class Unique {
    public int [] intUniqueList (int intList []) {
        int tempInt = intList[0];
        ArrayList<Integer> uniquesInt = new ArrayList<Integer>();
        for (int i = 1; i < intList.length; i++) {
            if (tempInt != intList[i]){
                uniquesInt.add(tempInt);
            }
            tempInt = intList[i];
        }
        int [] result =  new int [uniquesInt.size()];
        for (int i = 0; i < uniquesInt.size(); i++) {
            result[i] = uniquesInt.get(i);
        }
        return result;
    }
}
