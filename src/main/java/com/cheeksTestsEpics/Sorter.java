package com.cheeksTestsEpics;

import java.lang.reflect.Array;

/**
 * Created by EnriqueLC on 02/10/2016.
 */
public class Sorter {
    public int [] bubbleSort(int [] unsortedData) {
        int tempCurrentValue = 0;
        if (unsortedData!= null && unsortedData.length > 0) {
            //first iteration 5 compare 5
            for (int i = 0; i < unsortedData.length; i ++) {
                for (int j = 0; j < unsortedData.length; j ++) {
                    if (unsortedData[i] <= unsortedData[j]) {
                        tempCurrentValue = unsortedData[i];
                        unsortedData[i] = unsortedData[j];
                        unsortedData[j] = tempCurrentValue;
                    }
                }
            }
        }
        return unsortedData;
    }
}
